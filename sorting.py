from program import print_list

d=[23,56,45454,45,65,86,234,456,234,768,980,34]
names=["Anton","Bahaa","Pasha","Alisher","Sabina","MekuGirl"]
print_list(d)

print (".......................")
d=sorted(d)
print_list(d)

print (".......................")
print_list(sorted(names,reverse=True))

def my_sort(array):
    for i in range(len(array)):
        for j in range(i+1,len(array)):
            if array[i]>array[j]:
                array[i],array[j]=array[j],array[i]
    return array
my_sort(d)
print_list(d)